### Short Form Formulas for Statblocks

1. **Normal Statblock**:
   - **AC**: 12 + (1/2 × CR)
   - **HP**: 20 × CR
   - **Attack Bonus**: 3 + (1/2 × CR)
   - **DPR (Single-Target)**: 7 × CR (or 2d6 per CR)
   - **DPR (Multiple-Target)**: 3 × CR (or 1d6 per CR)
   - **Saving Throw (Proficiency)**: 3 + (1/2 × CR)
   - **Saving Throw DC**: 12 + (1/2 × CR)

2. **Artillery**:
   - **AC**: (12 + 1/2 × CR) - 2
   - **HP**: 20 × CR × 0.8
   - **Attack Bonus**: 3 + (1/2 × CR)
   - **DPR (Single-Target)**: 7 × CR × 0.6
   - **DPR (Multiple-Target)**: 3 × CR (or 1d6 per CR)
   - **Saving Throw**: (3 + 1/2 × CR) - 2
   - **AOE DPR**: 3 × CR (or 1d6 per CR)

3. **Brute**:
   - **AC**: (12 + 1/2 × CR) - 2
   - **HP**: 20 × CR × 1.2
   - **Attack Bonus**: (3 + 1/2 × CR) + 2
   - **DPR (Single-Target)**: 7 × CR × 1.2
   - **DPR (Multiple-Target)**: 3 × CR × 1.2
   - **Saving Throw**: 3 + (1/2 × CR)

4. **Support**:
   - **AC**: (12 + 1/2 × CR) + 2
   - **HP**: 20 × CR × 0.8
   - **Attack Bonus**: 3 + (1/2 × CR)
   - **DPR (Single-Target)**: 7 × CR × 0.6
   - **DPR (Multiple-Target)**: 3 × CR × 0.6
   - **Saving Throw**: (3 + 1/2 × CR) + 2

5. **Elite**:
   - **AC**: (12 + 1/2 × CR) + 1
   - **HP**: 20 × CR × 1.35
   - **Attack Bonus**: 3 + (1/2 × CR)
   - **DPR (Single-Target)**: 7 × CR × 1.35
   - **DPR (Multiple-Target)**: 3 × CR × 1.35
   - **Saving Throw**: (3 + 1/2 × CR) + 1
   - **AOE DPR**: 3 × CR (or 1d6 per CR)

6. **Minion**:
   - **AC**: (12 + 1/2 × CR) - 2
   - **HP**: 20 × CR × 0.4
   - **Attack Bonus**: 3 + (1/2 × CR)
   - **DPR (Single-Target)**: 7 × CR
   - **DPR (Multiple-Target)**: 3 × CR
   - **Saving Throw**: (3 + 1/2 × CR) - 2

7. **Boss**:
   - **AC**: (12 + 1/2 × CR)
   - **HP**: 20 × CR × 1.5
   - **Attack Bonus**: 3 + (1/2 × CR)
   - **DPR (Single-Target)**: 7 × CR × 1.5
   - **DPR (Multiple-Target)**: 3 × CR × 1.5
   - **Saving Throw**: (3 + 1/2 × CR) + 2
   - **AOE DPR**: 3 × CR (or 1d6 per CR)

8. **Uber Boss**:
   - **AC**: (12 + 1/2 × CR) + 2
   - **HP**: 20 × CR × 2
   - **Attack Bonus**: (3 + 1/2 × CR) + 2
   - **DPR (Single-Target)**: 7 × CR × 2
   - **DPR (Multiple-Target)**: 3 × CR × 2
   - **Saving Throw**: (3 + 1/2 × CR) + 3
   - **AOE DPR**: 3 × CR (or 1d6 per CR)

9. **Omega Boss**:
   - **AC**: (12 + 1/2 × CR) + 4
   - **HP**: 20 × CR × 3.5
   - **Attack Bonus**: (3 + 1/2 × CR) + 4
   - **DPR (Single-Target)**: 7 × CR × 2.5
   - **DPR (Multiple-Target)**: 3 × CR × 2.5
   - **Saving Throw**: (3 + 1/2 × CR) + 5
   - **AOE DPR**: 3 × CR × 1.5 (or 1d6 per CR × 1.5)