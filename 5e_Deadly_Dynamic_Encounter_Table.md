## Roll
1d20 per progress into the zone. Zones have 3 layers of progress.


# Tier 0 (1st Level)

1. Four CR 0 
2. Eight CR 0 
3. Eight CR 0 
4. Four CR 1/8 
5. Four CR 1/8 
6. Four CR 1/8 
7. Four CR 1/8 
8. Four CR 1/8 
9. Eight CR 1/8 
10. Eight CR 1/8 
11. Four CR 1/4 
12. Four CR 1/4 
13. Four CR 1/4 
14. Four CR 1/4 
15. Four CR 1/4 
16. Four CR 1/4 
17. Four CR 1/4 
18. Ten CR 1/4 
19. Ten CR 1/4 
20. Ten CR 1/4 
21. Ten CR 1/4 
22. Ten CR 1/4 
23. Ten CR 1/4 
24. Ten CR 1/4 
25. Ten CR 1/4 
26. Ten CR 1/4 
27. Ten CR 1/4 
28. Ten CR 1/4 
29. Eight CR 1/4 
30. Two CR 1/2 
31. Two CR 1/2 
32. Two CR 1/2 
33. Two CR 1/2 
34. Two CR 1/2 
35. Two CR 1/2 
36. Two CR 1/2 
37. Two CR 1/2 
38. Two CR 1/2 
39. Two CR 1/2 
40. Three CR 1/2 
41. Three CR 1/2 
42. Three CR 1/2 
43. Four CR 1/2 
44. Four CR 1/2 
45. Four CR 1/2 
46. One CR 1 
47. One CR 1 
48. One CR 1 
49. One CR 1 
50. One CR 1 
51. One CR 1 
52. One CR 1 
53. One CR 1
54. One CR 1 
55. One CR 1 
56. One CR 1 
57. One CR 1 
58. Two CR 1 
59. One CR 2 (at half HP)
60. One CR 2 (at half HP)


# Tier 1 (2nd to 4th)

1. One CR 1
2. Two CR 1/4 
3. Four CR 1/8
4. One CR 1  
5. Two CR 1/4 
6. Two CR 1/2
7. Four CR 1/4 
8. Two CR 1/2
9. One CR 2
10. Three CR 1/2
11. Six CR 1/4
12. Two CR 1
13. One CR 1 and Two CR 1/2
14. Three CR 1; OR One CR 2 and Three CR 1/2
15. Four CR 1/2
16. One CR 1 and Three CR 1/2
17. One CR 3 and One CR 1/2
18. Eight CR 1/4
19. One CR 1 and Six CR 1/4
20. Two CR 2
21. Two CR 1
22. One CR 2
23. Four CR 1/2
24. One CR 1 and Four CR 1/2
25. Four CR 1/2
26. Four CR 1
27. Eight CR 1/4
28. Three CR 1 and Four CR 1/2
29. One CR 3 and Eight CR 1/8
30. One CR 4
31. Two CR 3
32. Three CR 2
33. Two CR 2 and Four CR 1/2
34. One CR 4 and Six CR 1/4
35. One CR 4 and Two CR 1
36. Two CR 3 and One CR 2
37. Two CR 2 and Six CR 1/2
38. Two CR 2 and Four CR 1
39. Four CR 2
40. Two CR 4
41. One CR 5 and Three CR 1
42. One CR 5 and  Six CR 1/2
43. One CR 6 and Two CR 1
44. One CR 6 and Four CR 1/2
45. Eight CR 1
46. Two CR 2 and Eight CR 1/2
47. One CR 4 and Four CR 1
48. One CR 5 and Two CR 1 and Eight CR 1/8
49. One CR 6 and Two CR 1 and Eight CR 1/8
50. One CR 5 and Five CR 1
51. Two CR 3 and Two CR 2
52. One CR 4 and Three CR 2
53. One CR 4 and Six CR 1
54. Two CR 5
55. Two CR 5 and 8 CR 1/4
56. One CR 6 and Two CR 3
57. One CR 6 and One CR 3 and Six CR 1/2
58. One CR 5 and One CR 3 and Two CR 2
59. Two CR 5 and Six CR 1/2
60. Two CR 6





# Tier 2 (5th to 11th)



1. One CR 7 and 8 CR 1/8
2. Two CR 4
3. Four CR 2
4. One CR 7 and Four CR 1/2
5. One CR 6 and six CR 1
6. One CR 6 and Three CR 2
7. One CR 7 and One CR 3 and Four CR 1/2
8. One CR 7 and One CR 5
9. Two CR 5 and Four CR 1/2
10. One CR 8 and Four CR 1/2
11. One CR 9 and Eight CR 1/8
12. Two CR 5
13. One CR 5 and Ten CR 1/2
14. One CR 9 and Eight CR 1/8
15. Two CR 6 and Four CR 1
16. One CR 6 and Two CR 5
17. Four CR 4
18. Two CR 8
19. One CR 9 and One CR 7
20. One CR 9 and Two CR 3 and 8 CR 1/8
21. One CR 10 and Eight CR 1/8
22. Two CR 4 and Two CR 3 
23. One CR 9 and Four CR 2
24. Two CR 7 and Eight CR 1/8
25. One CR 8 and One CR 6
26. Four CR 4 and Four CR 2
27. Two CR 5 and Four CR 3
28. One CR 11
29. Two CR 6 and Four CR 3
30. One CR 9 and Eight CR 1/4
31. One CR 8 and Two CR 6
32. Three CR 5 and Two CR 3
33. Four CR 4 and Three CR 3
34. One CR 9 and Four CR 2
35. Two CR 7 and One CR 5
36. One CR 9 and One CR 8
37. Two CR 7 and Three CR 4
38. One CR 10 and One CR 7
39. Three CR 6 and Two CR 3
40. One CR 11 and Two CR 6
41. One CR 12 and One CR 10
42. Two CR 9 and Two CR 4
43. Two CR 7 and One CR 6
44. One CR 13 and One CR 9
45. Two CR 8 and One CR 4
46. One CR 12 and Two CR 7
47. One CR 14 and One CR 8
48. Two CR 9 and One CR 7
49. One CR 11 and One CR 8
50. One CR 13 and Two CR 6
51. One CR 13 and One CR 10
52. One CR 10 and Two CR 7
53. Three CR 8 and One CR 5
54. One CR 12 and Two CR 7
55. One CR 15 and One CR 9
56. One CR 9 and Two CR 8
57. One CR 13 and One CR 11
58. One CR 10 and Two CR 9
59. One CR 14 and One CR 10
60. Two CR 11 and One CR 7













# Tier 3 (12th to 17th)

1. One CR 14 and One CR 9
2. One CR 15 and One CR 8
3. Two CR 12 and One CR 6
4. One CR 16 and One CR 8
5. Three CR 10
6. One CR 14 and One CR 10
7. Two CR 13 and One CR 5
8. One CR 17
9. Two CR 12 and Two CR 4
10. One CR 16 and One CR 7
11. One CR 15 and One CR 9
12. Two CR 14
13. One CR 17
14. One CR 16 and One CR 11
15. Two CR 13 and One CR 6
16. Three CR 11
17. One CR 15 and One CR 9
18. One CR 18
19. Two CR 13 and One CR 7
20. One CR 14 and One CR 10
21. One CR 18 and One CR 10
22. Two CR 16 and One CR 5
23. One CR 19 and One CR 8
24. One CR 17 and Two CR 9
25. One CR 20 and One CR 7
26. One CR 19 and One CR 6
27. Two CR 15 and Two CR 7
28. One CR 16 and Two CR 10
29. Two CR 17 and One CR 3
30. One CR 18 and Two CR 6
31. One CR 21 and One CR 7
32. One CR 22 and One CR 6
33. Two CR 19
34. One CR 18 and Two CR 11
35. Three CR 12 and One CR 4
36. One CR 20 and One CR 10
37. Two CR 18 and One CR 4
38. One CR 19 and Two CR 9
39. One CR 22 and One CR 3
40. Two CR 16 and One CR 8
41. One CR 23 and One CR 5
42. Two CR 20 and One CR 2
43. One CR 22 and One CR 8
44. One CR 24 and One CR 4
45. Three CR 14
46. One CR 21 and One CR 9
47. Two CR 18 and One CR 6
48. One CR 22 and One CR 5
49. One CR 20 and One CR 7 and One CR 4
50. Two CR 19 and One CR 3
51. One CR 25 and One CR 2
52. Two CR 22 and One CR 1
53. One CR 23 and Two CR 11
54. One CR 24 and One CR 9
55. Three CR 15
56. One CR 25 and Two CR 5
57. Two CR 22 and One CR 2
58. One CR 24 and Two CR 10
59. Two CR 23 and One CR 9
60. Three CR 16








# Tier 4 (18th to 20th)

1. One CR 23 and One CR 18 
2. One CR 24 and Two CR 10 
3. One CR 24 and One CR 17 
4. One CR 24 and Three CR 9 
5. One CR 24 and Two CR 12 and Six CR 5 
6. One CR 25 and One CR 16 
7. One CR 25 and Two CR 12 and Six CR 6 
8. Two CR 25 and One CR 9 
9. One CR 26 and One CR 11 
10. One CR 26 and Two CR 7 
11. One CR 26 and Two CR 13 and Six CR 6 
12. One CR 26 and Two CR 13 and Six CR 6 
13. One CR 26 and Two CR 13 and Six CR 6 
14. Two CR 26 and Two CR 7 
15. One CR 27 and One CR 5 
16. One CR 27 and One CR 8 
17. One CR 27 and Two CR 13 and Six CR 6 
18. One CR 27 and Two CR 13 and Six CR 6 
19. One CR 27 and Two CR 13 and Six CR 6 
20. Three CR 27 
21. One CR 23 and One CR 20 
22. Two CR 23 and One CR 14 
23. One CR 24 and Two CR 12 and Six CR 6 
24. Two CR 24 and One CR 11 
25. One CR 25 and One CR 20 
26. Two CR 25 and One CR 11 
27. One CR 26 and Two CR 13 and Six CR 6 
28. Two CR 26 and Two CR 7 
29. One CR 27 and One CR 10 
30. One CR 27 and Two CR 13 and Six CR 6 
31. Two CR 27 and One CR 5 
32. Two CR 27 and Two CR 7 
33. One CR 28 and One CR 6 
34. One CR 28 and One CR 14 and Six CR 7
35. One CR 28 and Two CR 14 and Six CR 7
36. One CR 28 and Two CR 14 and Six CR 7
37. One CR 28 and Two CR 8
38. Two CR 28 and One CR 7 
39. One CR 28 and Two CR 14 and Six CR 7 
40. Two CR 28 and Two CR 8 
41. One CR 23 and Two CR 11 and Six CR 5 
42. One CR 24 and Two CR 12 and Six CR 6 
43. One CR 24 and Two CR 12 and Six CR 6 
44. One CR 24 and Two CR 12 and Six CR 6 
45. One CR 25 and Two CR 12 and Six CR 6 
46. One CR 25 and Two CR 12 and Six CR 6 
47. One CR 25 and Two CR 12 and Six CR 6 
48. One CR 26 and Two CR 13 and Six CR 6 
49. One CR 26 and Two CR 13 and Six CR 6 
50. One CR 26 and Two CR 13 and Six CR 6 
51. One CR 27 and Two CR 13 and Six CR 6 
52. One CR 27 and Two CR 13 and Six CR 6 
53. One CR 27 and Two CR 13 and Six CR 6 
54. One CR 28 and Two CR 14 and Six CR 7 
55. One CR 28 and Two CR 14 and Six CR 7 
56. One CR 28 and Two CR 14 and Six CR 7 
57. One CR 29 and Two CR 14 and Six CR 7 
58. One CR 29 and Two CR 14 and Six CR 7 
59. One CR 29 and Two CR 14 and Six CR 7 
60. One CR 30 and Two CR 15 and Six CR 7
