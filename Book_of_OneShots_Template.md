# Adventure Name

### Adventure Overview

Summary: 

  

One Sentence Summary: 

- Starting at V, the PCs will W at (locations and areas), while contending with X, searching for Y, to the climax Z.  
      
    

Paragraph Summary:

- In [setting], a group of adventurers are brought together by [inciting event or quest giver].
    
- Their mission is to [primary objective or goal], but they soon discover that [unexpected twist or complication].
    
- As they journey through [key locations], they face [primary challenges or antagonists].
    
- In the end, the heroes must [final challenge or climax] to achieve [ultimate goal or resolution].
    

  

### Game Master Notes

  

Level.    Text

Objective.    Text

Creatures.     Text

Resting Parameters.   Short rests:   Long Rests:

Short Rest Opportunities?: Y/N, “After X, at Y, if the PCs Z.””Parameter”

Intended Climax: Reveal/Boss 

Loot. Text

  
  
  

### Background Storyline and Calendar

Background

Current Situation

Aftermath

  

### Locations Textbox

Location1. General sensory description, purpose, special notes

Location2. General sensory description, purpose, special notes

Location3. General sensory description, purpose, special notes

  

### NPCs

NPC Name

Race, Sex, Alignment, Role/ Class Level

Description.

Occupation. 

Secret.

  

### Encounters

Level. Text

Creatures. Text

Location. Text

Description. Text

Loot.

- Loot by Monster
    
- Main Collectible
    
- Secret Collectible
    

Secret. Text

  
  

Description. Text

Loot. Text

Secret. Text

  

## Running the Adventure

Preamble To Game

Text to be read by DM to players for start of adventure

  

Area 1: Initial Conflict, Solve Complication

Text to be read by DM

Area Detail . Text

Traps & Hazards

- Type of Trap (Complexity of Trap, level of trap, how dangerous is the trap)
    
- Description. Text 
    
- Trigger. Text
    
- Effect. Text
    

Encounter

- Statbl0ck details. 
    

  

Transition Area 2: Setback/Hazards

Text to be read by DM

Area Detail . Text

Traps & Hazards

- Type of Trap (Complexity of Trap, level of trap, how dangerous is the trap)
    
- Description. Text 
    
- Trigger. Text
    
- Effect. Text
    

Encounter

- Statbl0ck details. 
    

  
  

Area 3: Big Reveal, Significant Combat, Loot

Text to be read by DM

Area Detail . Text

Traps & Hazards

- Type of Trap (Complexity of Trap, level of trap, how dangerous is the trap)
    
- Description. Text 
    
- Trigger. Text
    
- Effect. Text
    

Encounter

- Statbl0ck details. 
    

  

Conclusion:
