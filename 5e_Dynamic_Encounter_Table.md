
## Roll
1d20 per progress into the zone. Zones have 3 layers of progress.


# Tier 0 (1st)

- CR 0 to 1/4: One monster per character
- CR 1/2: One monster per two characters
- CR 1: One monster per four characters

1. One per PC CR 0  
2. Two per PC CR 0
3. Two per PC CR 0
4. One per PC CR 1/8 
5. One per PC CR 1/8
6. One per PC CR 1/8
7. One per PC CR 1/8
8. One per PC CR 1/8
9. Two plus One per PC CR 1/8
10. Two per PC CR 1/8
11. One per PC CR 1/4
12. One per PC CR 1/4
13. One per PC CR 1/4
14. One per PC CR 1/4
15. One per PC CR 1/4
16. One per PC CR 1/4
17. One per PCCR 1/4
18. Two plus One per PC CR 1/4
19. Two plus One per PC CR 1/4
20. Two plus One per PC CR 1/4
21. Two plus One per PC CR 1/4
22. Two plus One per PC CR 1/4
23. Two plus One per PC CR 1/4
24. Two plus One per PC CR 1/4
25. Two plus One per PC CR 1/4
26. Two plus One per PC CR 1/4
27. Two plus One per PC CR 1/4
28. Two plus One per PC CR 1/4
29. Two per PC CR 1/4
30. One per Two PCs CR 1/2
31. One per Two PCs  CR 1/2
32. One per Two PCs CR 1/2
33. One per Two PCs CR 1/2
34. One per Two PCs CR 1/2
35. One per Two PCs CR 1/2
36. One per Two PCs CR 1/2
37. One per Two PCs CR 1/2
38. One per Two PCs CR 1/2
39. One per Two PCs CR 1/2
40. One per Two PCs plus One CR 1/2
41. One per Two PCs plus One CR 1/2
42. One per Two PCs plus One CR 1/2
43. One per Two PCs plus Two CR 1/2
44. One per Two PCs plus Two CR 1/2
45. One per PC CR 1/2
46. One per four PCs CR 1
47. One per four PCs CR 1
48. One per four PCs CR 1
49. One per four PCs CR 1
50. One per four PCs CR 1
51. One per four PCs CR 1
52. One per four PCs CR 1
53. One per four PCs CR 1
54. One CR 1
55. One CR 1
56. One CR 1
57. One CR 1
58. Two CR 1
59. One 1/2 HP CR 2
60. One 1/2 HP CR 2


# Tier 1 (2nd to 4th)

- CR = 1/10 level: Two monsters per character
- CR = 1/4 level: One monster per character
- CR = 1/2 level: One monster per two characters
- CR = Level: One monster per four characters

1. Two per PC CR 1/4  
2. Two per PC CR 1/4 
3. Two per PC CR 1/4 
4. Two per PC CR 1/4  
5. Two per PC CR 1/4 
6. Two per PC CR 1/4 
7. Two per PC CR 1/4 
8. Two per PC CR 1/4 
9. One per PC CR 1/2
10. One per PC CR 1/2
11. One per PC CR 1/2
12. One per PC CR 1/2
13. One per PC CR 1/2
14. Two per PC CR 1/2
15. Two per PC CR 1/2
16. Two per PC CR 1/2
17. Two per PC CR 1/2
18. Two per PC CR 1/2
19. Two per PC CR 1/2
20. One per Two PCs CR 1
21. One per Two PCs CR 1
22. One per Two PCs CR 1
23. One per Two PCs CR 1
24. One per Two PCs CR 1
25. One per Two PCs CR 1
26. One per PC CR 1
27. One per PC CR 1
28. One per PC CR 1
29. One per PC CR 1
30. One per PC CR 1
31. One per PC CR 1
32. One per PC CR 1
33. One per PC CR 1
34. One per PC CR 1
35. One per PC CR 1
36. One per PC CR 1
37. One per PC CR 1
38. One per PC CR 1
39. One per PC CR 1
40. One per Two PCs CR 2
41. One per Two PCs CR 2
42. One per Two PCs CR 2
43. One per Two PCs CR 2
44. One per Two PCs CR 2
45. One per PC CR 2
46. One per four PCs CR 3
47. One per four PCs CR 3
48. One per four PCs CR 3
49. One per four PCs CR 3
50. One per three PCs CR 3
51. One per three PCs CR 3
52. One per two PCs CR 3
53. One per two PCs CR 3
54. One CR 4
55. One CR 4
56. One CR 4
57. One CR 4
58. Two CR 3
59. One CR 5
60. One CR 6





# Tier 2 (5th to 11th)

- CR = 1/10 level: Four monsters per character
- CR = 1/4 level: Two monsters per character
- CR = 1/2 level: One monster per character
- CR = 3/4 level: One monster per two characters
- CR = Level + 3: One monster per four characters




1. Four per PC CR 1/2
2. Four per PC CR 1/2
3. Four per PC CR 1/2
4. Four per PC CR 1/2
5. Four per PC CR 1/2
6. Four per PC CR 1/2
7. Four per PC CR 1/2
8. Four per PC CR 1/2
9. Two per PC CR 1
10. Two per PC CR 1
11. Two per PC CR 1
12. Two per PC CR 1
13. Two per PC CR 1
14. Two per PC CR 1
15. Two per PC CR 1
16. Two per PC CR 1
17. Two per PC CR 1
18. Two per PC CR 1
19. One per PC CR 2
20. One per PC CR 2
21. One per PC CR 2
22. One per PC CR 2
23. One per PC CR 2
24. One per PC CR 2
25. One per PC CR 2
26. One per PC CR 2
27. One per PC CR 2
28. One per PC CR 2
29. One per PC CR 2
30. One per PC CR 2
31. One per Two PCs CR 3
32. One per Two PCs CR 3
33. One per Two PCs CR 3
34. One per Two PCs CR 3
35. One per Two PCs CR 3
36. One per Two PCs CR 3
37. One per Two PCs CR 3
38. One per PC CR 3
39. One per PC CR 3
40. One per Two PCs CR 4
41. One per Two PCs CR 4
42. One per Two PCs CR 4
43. One per Four PCs CR 8
44. One per Four PCs CR 8
45. One per Four PCs CR 8
46. One per Four PCs CR 8
47. One per Four PCs CR 8
48. One per Four PCs CR 8
49. One per Four PCs CR 8
50. One per Four PCs CR 8
51. One per Four PCs CR 9
52. One per Four PCs CR 9
53. One per Four PCs CR 9
54. One per Four PCs CR 9
55. One per Four PCs CR 9
56. One per Four PCs CR 9
57. One CR 10
58. One CR 10
59. One CR 10
60. One CR 11












# Tier 3 (12th to 17th)

- CR = 1/10 level: Four monsters per character
- CR = 1/4 level: Two monsters per character
- CR = 1/2 level: One monster per character
- CR = 3/4 level: One monster per two characters
- CR = Level + 3: One monster per four characters



1. Four per PC CR 1
2. Four per PC CR 1
3. Four per PC CR 1
4. Four per PC CR 1
5. Four per PC CR 1
6. Four per PC CR 1
7. Four per PC CR 1
8. Four per PC CR 1
9. Two per PC CR 3
10. Two per PC CR 3
11. Two per PC CR 3
12. Two per PC CR 3
13. Two per PC CR 3
14. Two per PC CR 3
15. Two per PC CR 3
16. Two per PC CR 3
17. One per PC CR 5
18. One per PC CR 5
19. One per PC CR 5
20. One per PC CR 5
21. One per PC CR 5
22. One per PC CR 5
23. One per PC CR 5
24. One per PC CR 5
25. One per Two PCs CR 6
26. One per Two PCs CR 6
27. One per Two PCs CR 6
28. One per Two PCs CR 6
29. One per Two PCs CR 6
30. One per Two PCs CR 6
31. One per Two PCs CR 8
32. One per Two PCs CR 8
33. One per Two PCs CR 8
34. One per Two PCs CR 8
35. One per Two PCs CR 8
36. One per Two PCs CR 8
37. One per Two PCs CR 9
38. One per Two PCs CR 9
39. One per Two PCs CR 9
40. One per Two PCs CR 9
41. One per Four PCs CR 12
42. One per Four PCs CR 12
43. One per Four PCs CR 12
44. One per Four PCs CR 12
45. One per Four PCs CR 12
46. One per Four PCs CR 12
47. One per Four PCs CR 13
48. One per Four PCs CR 13
49. One per Four PCs CR 13
50. One per Four PCs CR 13
51. One CR 15
52. One CR 15
53. One CR 15
54. One CR 15
55. One CR 16
56. One CR 16
57. One CR 17
58. One CR 17
59. One CR 18
60. One CR 19








# Tier 4 (18th to 20th)

- CR = 1/10 level: Four monsters per character
- CR = 1/4 level: Two monsters per character
- CR = 1/2 level: One monster per character
- CR = 3/4 level: One monster per two characters
- CR = Level + 3: One monster per four characters



1. Four per PC CR 5
2. Four per PC CR 5
3. Four per PC CR 5
4. Four per PC CR 5
5. Four per PC CR 5
6. Four per PC CR 5
7. Four per PC CR 5
8. Four per PC CR 5
9. Two per PC CR 9
10. Two per PC CR 9
11. Two per PC CR 9
12. Two per PC CR 9
13. Two per PC CR 9
14. Two per PC CR 9
15. Two per PC CR 9
16. Two per PC CR 9
17. One per PC CR 10
18. One per PC CR 10
19. One per PC CR 10
20. One per PC CR 10
21. One per PC CR 10
22. One per PC CR 10
23. One per PC CR 10
24. One per PC CR 10
25. One per Two PCs CR 13
26. One per Two PCs CR 13
27. One per Two PCs CR 13
28. One per Two PCs CR 13
29. One per Two PCs CR 13
30. One per Two PCs CR 13
31. One per Two PCs CR 15
32. One per Two PCs CR 15
33. One per Two PCs CR 15
34. One per Two PCs CR 15
35. One per Two PCs CR 15
36. One per Two PCs CR 15
37. One per Four PCs CR 21
38. One per Four PCs CR 21
39. One per Four PCs CR 21
40. One per Four PCs CR 21
41. One per Four PCs CR 21
42. One per Four PCs CR 21
43. One per Four PCs CR 22
44. One per Four PCs CR 22
45. One per Four PCs CR 22
46. One per Four PCs CR 22
47. One per Four PCs CR 22
48. One per Four PCs CR 22
49. One CR 23
50. One CR 23
51. One CR 23
52. One CR 23
53. One CR 23
54. One CR 23
55. One CR 23
56. One CR 23
57. One CR 23
58. One CR 23
59. One CR 23
60. One CR 23
