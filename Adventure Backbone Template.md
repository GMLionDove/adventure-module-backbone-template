# Adventure Module Backbone Template




# About The Adventure 

## ------------



### Six Truths of the Setting
1.
2.
3.
4.
5.
6.

### Required player Buy-In
1.
2.
3.
4.
5.
6.

\column
:::

### General Introduction (For PCs)
1. A Brief History
2. Starting Levels for PCs
3. Adventure Hooks

#### Region Gazetteer (For PCs)
1. Local Religions
2. Iconic Locations
3. Towns and Settlements
4. Rumors 
5. Public Factions



\page

# GM's Campaign Considerations

### Themes; their Motifs and Thesis
#### **Theme**:

**Motifs**: Motif, Motif, Motif 

**Thesis Statement**

#### **Theme**:

**Motifs**: Motif, Motif, Motif 

**Thesis Statement**

#### **Theme**:

**Motifs**: Motif, Motif, Motif 

**Thesis Statement**

#### **Theme**:

**Motifs**: Motif, Motif, Motif 

**Thesis Statement**

#### **Theme**:

**Motifs**: Motif, Motif, Motif 

**Thesis Statement**


\column


### Three Antagonist Plots

#### **The Mastermind's Plot**
* **Who:** Mastermind & Victims
* **What:**
* **When:**
* **Where:**
* **Why:**
* **How:**


#### **The Formidable Foe's Plot**
* **Who:** The Foe & The Prey
* **What:**
* **When:**
* **Where:**
* **Why:**
* **How:**

#### **The Patron's Hidden Agenda**
* **Who:** The Pary's Patron
* **What:**
* **When:**
* **Where:**
* **Why:**
* **How:**

##### **How to foreshadow the threats success?**
1. Mastermind Victory
2. Formidible Foe Victory
3. Patron's Agenda Unfolds


\page

## Plots W's

### Whos
**Three Antagonists** (pick a BBEG)
1. MASTERMIND:
2. THE SUBLEADER:
3. THE TRAITOR:
4. THE RIVAL PARTY:


**FORMIDABLE FOES**

BIG MONSTER:

WEIRD MONSTER:

TRAP MONSTER:

DRAGON:


###### **One Patron**


###### Two Neutral Faction Heads


###### Three Merchants
1. **Mundane**:
2. **Magic**:
3. **Crimminal**:

###### **One Faction preventing BBEG Victory**
1. LEADER:
2. MEANS TO PREVENT:



### Whats
**One Macguffin**

**3 or 5 Plot Coupons**

**Players' Stakes**
(*EVERY PLAYER HAS SOMETHING TO LOSE*)

###### Players' Rewards
1. Three Warrior Rewards
2. Three Mage Rewards
3. Two Thief Rewards
4. *Either* Two Holy Rewards 
4. *or* One Holy & One Dark
5. Four Party/Shareable Rewards

**Mastermind's Resources**


**Patron's Resources**


**Means to Prevent BBEG**

\column



:::
### Whens
**When Nemesis needs Macguffin by:**


**When STAKES are first threatened:**


**When PCs need plot devices by:**

**Macguffin**:

**Plot Coupons**


### Where
**Setting**
1. WORLD: 
2. REGION:
3. CITIES:

**Inciting incident**:


**Possible Party Starts**:


**Medium Sized City**


**General Adventure Direction(NESW)**:


**LAIRS**:
1. MASTERMIND:
2. THE SUBLEADER:
3. BIG MONSTER:
4. WEIRD MONSTER:
5. TRAP MONSTER:
6. DRAGON:

### Whys
1. MASTERMIND:
2. THE SUBLEADER:
3. TRAITOR:
4. PATRON:
5. DRAGON:
6. FACTIONS:

\page

## 3 ACT FLOWCHART

### 1. Inciting incident.
Who NPC:
Who Antagonist:
Object of Conflict:
Rewards:
Hooks to next Scene:
Must Resolves:

#### Scene Setup
1. **Dramatic Act:**
2. **Strong Start:** 
3. **Potential Scene Transitions:** 
#### **Core Scene Aspects:**
   - **Fail-state:** 
   - **Tempo:** 
   - **Amaze:** 
   - **Counter:** 
   - **Danger:** 
   - **Tactical Utility:** 
#### **Tension Escalation:**
   - **Hazard:** 
   - **Stakes**: 
   - **Complication:** 
   - **Counter:** 
#### **Extra Prep**
   * **Secrets and Clues:** 
   * **Memorable Scenery:** 
   * **Talkie Puzzle:** 
   * **Boss and Magic Item:** 
   * **Hook the Next Scene:** 
#### Step 3: Seven Node Geomorph
1. **Setup:** 
2. **Entrance:**
3. **Puzzle:** 
4. **Setback:** 
5. **Battle:** 
6. **Reward:** 
7. **"Empty Space":** 



\column

:::

### 2. Threshold of Stakes
Who NPC:
Who Antagonist:
Object of Conflict:
Rewards:
Hooks to next Scene:
Must Resolves:

#### Scene Setup
1. **Dramatic Act:**
2. **Strong Start:** 
3. **Potential Scene Transitions:** 
#### **Core Scene Aspects:**
   - **Fail-state:** 
   - **Tempo:** 
   - **Amaze:** 
   - **Counter:** 
   - **Danger:** 
   - **Tactical Utility:** 
#### **Tension Escalation:**
   - **Hazard:** 
   - **Stakes**: 
   - **Complication:** 
   - **Counter:** 
#### **Extra Prep**
   * **Secrets and Clues:** 
   * **Memorable Scenery:** 
   * **Talkie Puzzle:** 
   * **Boss and Magic Item:** 
   * **Hook the Next Scene:** 
#### Step 3: Seven Node Geomorph
1. **Setup:** 
2. **Entrance:**
3. **Puzzle:** 
4. **Setback:** 
5. **Battle:** 
6. **Reward:** 
7. **"Empty Space":** 


\page

### 3. Rising Action
Who NPC:
Who Antagonist:
Object of Conflict:
Rewards:
Hooks to next Scene:
Must Resolves:

#### Scene Setup
1. **Dramatic Act:**
2. **Strong Start:** 
3. **Potential Scene Transitions:** 
#### **Core Scene Aspects:**
   - **Fail-state:** 
   - **Tempo:** 
   - **Amaze:** 
   - **Counter:** 
   - **Danger:** 
   - **Tactical Utility:** 
#### **Tension Escalation:**
   - **Hazard:** 
   - **Stakes**: 
   - **Complication:** 
   - **Counter:** 
#### **Extra Prep**
   * **Secrets and Clues:** 
   * **Memorable Scenery:** 
   * **Talkie Puzzle:** 
   * **Boss and Magic Item:** 
   * **Hook the Next Scene:** 
#### Step 3: Seven Node Geomorph
1. **Setup:** 
2. **Entrance:**
3. **Puzzle:** 
4. **Setback:** 
5. **Battle:** 
6. **Reward:** 
7. **"Empty Space":** 

\column

### 4. Climax and Cliffhanger
Who NPC:
Who Antagonist:
Object of Conflict:
Rewards:
Hooks to next Scene:
Must Resolves:

#### Scene Setup
1. **Dramatic Act:**
2. **Strong Start:** 
3. **Potential Scene Transitions:** 
#### **Core Scene Aspects:**
   - **Fail-state:** 
   - **Tempo:** 
   - **Amaze:** 
   - **Counter:** 
   - **Danger:** 
   - **Tactical Utility:** 
#### **Tension Escalation:**
   - **Hazard:** 
   - **Stakes**: 
   - **Complication:** 
   - **Counter:** 
#### **Extra Prep**
   * **Secrets and Clues:** 
   * **Memorable Scenery:** 
   * **Talkie Puzzle:** 
   * **Boss and Magic Item:** 
   * **Hook the Next Scene:** 
#### Step 3: Seven Node Geomorph
1. **Setup:** 
2. **Entrance:**
3. **Puzzle:** 
4. **Setback:** 
5. **Battle:** 
6. **Reward:** 
7. **"Empty Space":** 


