## Power relative to Party
Nicknames for Foe Danger levels.

##### Pawn
One trick ponies. Any solid hit will do them in. They do a single thing and are then expendable. Ignore them however, and they will control the scene.

##### Knave
The standard foe. One or two tricks up their sleeve, two or three hits do them in, are a solid danger in groups or when being led.

### Jack
Jacks come in 3 varieties based on their team-up. 
##### Chariot Jack
Drives a small group of Knaves and/or Pawns. 
##### Lance Jack
Accompanies a Knight. Often a complementing opposite of the Knight, sometimes with Pawns that are mini-copycats of the Knight.
##### Porter Jack
Accompanies a Royal or an Ace, often is some kind of resource carrier or manipulates terrain in the zone or scene.

### Knight
A party will have to carefully consider who and how many will contend with a single Knight. Knight's are the lesser of the miniboss foes. Called a quirky miniboss squad by some, there should only ever be enough that the party can still take them on, two PCs to one Knight at least.
Knights are Quirky. Have fun with their design, aesthetic, and powers.

### Royals
Ussualy comes in a pair or as a two stage Foe. Royals are not intended to be Gendered and are referred to via chess pieces.
##### Queen 
The Aggressive Heavy Hitter. Fast and strong without any real weaknesses.
Archetypal of the ethics and aesthetic of their Banner.
##### King
The Evasive Supporter. Has one or two weak areas and a particularly strong area. Very often is an opposite compliment of the Queen and heals or buffs the Queen.
Is often a stark contrast to the aesthetics and ethics their own Banner.

### Ace
The Ace doesn't need to team up, but will sometimes call upon a mob of Pawns or even a Knight and a Jack while they take a step back. The Ace will either resemble a greater version of a King or Knight in terms of Banner theme, but there will always be at least one greater-queen type of Ace, the Standard Bearer.



# Combat Role

#### Hearts
Healer
#### Diamonds
Support
#### Clubs
Striker
#### Spades
Tank



## Rooks
Rooks are tough, direct, and tend towards nonmagical.
##### Hearts Rook
Tough and Heals Allies, maybe has regen or a second phase.
##### Diamonds Rook
Tough and Buffs Allies, maybe procs allied specials.
##### Clubs Rook
Tough and hits hard, is strong all around.
##### Spades Rook
The toughest, will have some debuffs for enemies to draw attention, and will have serious mobility.



## Bishops
Bishops are evasive, crafty, and tend towards magical
##### Hearts Bishop
Evasive and Heals allies, probably has buffs and debuffs as well. 
##### Diamonds Bishop
Evasive and buffs allies, will debuff enemies, and probably deal damage over time. Maybe has a heal or two.
##### Clubs Bishop
Evasive and hits the hardest. Probably has some damage negation like hiding or a shield, and more than likely is ranged with a single aoe.
##### Spades Bishop
Evasive, tougher than other Bishops, with aoe buffs and debuffs, and usually some ranged or skirmishing attack. A potentially annoying foe.



## The Board
Weak all around, but with many tricky (or a few very powerful) battlefield control effects, as well as using negates as their main defense.

